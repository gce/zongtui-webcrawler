package com.zongtui.fourinone.contractor;

import com.zongtui.fourinone.obj.ObjectBean;
import com.zongtui.fourinone.park.ParkPatternExector;

import java.util.ArrayList;
import java.util.List;

/**
 * 并行服务接口.
 */
public abstract class ParallelService {

    /**
     * 待命接口【远端】
     *
     * @param host       主机号
     * @param port       端口号
     * @param workerType 工作类型.
     */
    abstract public void waitWorking(String host, int port, String workerType);

    /**
     * 待命接口【本地】
     *
     * @param workerType 工作类型
     */
    abstract public void waitWorking(String workerType);


    /**
     * 获取
     *
     * @param parkHost
     * @param parkPort
     * @param workerType
     * @return
     */
    List<String[]> getWorkersServicePark(String parkHost, int parkPort, String workerType) {
        List<ObjectBean> oblist = ParkPatternExector.getWorkerTypeList(parkHost, parkPort, workerType);
        List<String[]> wslist = new ArrayList<>();
        for (ObjectBean ob : oblist) {
            String[] hostport = ((String) ob.toObject()).split(":");
            if (!hostport[0].equals(null) || Integer.parseInt(hostport[1]) != 0)
                wslist.add(new String[]{hostport[0], hostport[1], workerType});

        }
        return wslist;
    }

    List<String[]> getWorkersService(String host, int port, String workerType) {

        List<ObjectBean> oblist = ParkPatternExector.getWorkerTypeList(workerType);//getWorkerTypeList(String host, int port, String workerType)
        List<String[]> wslist = new ArrayList<>();
        for (ObjectBean ob : oblist) {
            String[] hostport = ((String) ob.toObject()).split(":");
            if (!hostport[0].equals(host) || Integer.parseInt(hostport[1]) != port)//&&
                wslist.add(new String[]{hostport[0], hostport[1], workerType});

        }
        return wslist;
    }

    protected List<String[]> getWorkersService(String workerType) {
        return getWorkersService(null, 0, workerType);
    }
}