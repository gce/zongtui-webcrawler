package com.zongtui.fourinone.park;

import com.zongtui.fourinone.obj.ObjectBean;
import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.exception.RecallException;

public class ParkPatternBean
{
	String domain,node;
	WareHouse inhouse,outhouse;
	ObjectBean thisversion;
	RecallException rx;
	public ParkPatternBean(String domain, String node, WareHouse inhouse, WareHouse outhouse, RecallException rx)
	{
		this.domain = domain;
		this.node = node;
		this.inhouse = inhouse;
		this.outhouse = outhouse;
		this.rx = rx;
	}
}