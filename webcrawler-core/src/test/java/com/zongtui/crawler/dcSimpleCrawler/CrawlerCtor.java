package com.zongtui.crawler.dcSimpleCrawler;


import com.zongtui.fourinone.contractor.Contractor;
import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.worker.WorkerLocal;

public class CrawlerCtor extends Contractor
{
	public WareHouse giveTask(WareHouse inhouse)
	{
		WorkerLocal[] wks = getWaitingWorkers("CrawlerWorker");
		System.out.println("爬虫工人数量:"+wks.length);
		
		WareHouse wh = new WareHouse("word", "你好，爬虫工人，开始干活吧！");
		WareHouse result = wks[0].doTask(wh);

		while(true){
			if(result.getStatus()==WareHouse.READY)
			{
				System.out.println("result:"+result);
				break;
			}
		}

		return null;
	}
	
	public static void main(String[] args)
	{
		CrawlerCtor a = new CrawlerCtor();
		a.giveTask(null);
	}
}