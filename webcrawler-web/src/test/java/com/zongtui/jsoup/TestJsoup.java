/**
 * Project Name:webcrawler-web
 * File Name:TestJsoup.java
 * Package Name:com.zongtui.jsoup
 * Date:2015-5-6下午1:34:58
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */

package com.zongtui.jsoup;

import java.io.IOException;

import junit.framework.Assert;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

/**
 * ClassName: TestJsoup <br/>
 * Function: jsoup测试. <br/>
 * date: 2015-5-6 下午1:34:59 <br/>
 * 
 * @author feng
 * @version
 * @since JDK 1.7
 */
public class TestJsoup {

	/**
	 * test:•	没有关闭的标签 (比如： <p>Lorem <p>Ipsum parses to <p>Lorem</p> <p>Ipsum</p>)
•	隐式标签 (比如. 它可以自动将 <td>Table data</td>包装成<table><tr><td>?)
•	创建可靠的文档结构（html标签包含head 和 body，在head只出现恰当的元素）
 <br/>
	 *
	 * @author feng
	 * @since JDK 1.7
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void test() {
		Document doc = null;
		try {
			doc = Jsoup.connect("http://www.baidu.com/").get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String title = doc.title();
		Assert.assertEquals("百度一下，你就知道", title);
	}

}
