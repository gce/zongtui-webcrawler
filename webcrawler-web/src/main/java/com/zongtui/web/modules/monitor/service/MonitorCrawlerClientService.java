/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.monitor.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zongtui.web.common.persistence.Page;
import com.zongtui.web.common.service.CrudService;
import com.zongtui.web.modules.monitor.entity.MonitorCrawlerClient;
import com.zongtui.web.modules.monitor.dao.MonitorCrawlerClientDao;

/**
 * 客户端监控Service
 * @author zhangfeng
 * @version 2015-05-02
 */
@Service
@Transactional(readOnly = true)
public class MonitorCrawlerClientService extends CrudService<MonitorCrawlerClientDao, MonitorCrawlerClient> {

	public MonitorCrawlerClient get(String id) {
		return super.get(id);
	}
	
	public List<MonitorCrawlerClient> findList(MonitorCrawlerClient monitorCrawlerClient) {
		return super.findList(monitorCrawlerClient);
	}
	
	public Page<MonitorCrawlerClient> findPage(Page<MonitorCrawlerClient> page, MonitorCrawlerClient monitorCrawlerClient) {
		return super.findPage(page, monitorCrawlerClient);
	}
	
	@Transactional(readOnly = false)
	public void save(MonitorCrawlerClient monitorCrawlerClient) {
		super.save(monitorCrawlerClient);
	}
	
	@Transactional(readOnly = false)
	public void delete(MonitorCrawlerClient monitorCrawlerClient) {
		super.delete(monitorCrawlerClient);
	}
	
}